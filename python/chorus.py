import copy as cp
import itertools as it

import abjad as aj

from .artifacts.artifact import Artifact
from .artifacts.bow import FastLegno, Hair, SlowLegno
from .artifacts.finger import Finger
from .artifacts.milk import MilkOff, MilkOn
from .artifacts.pick import ArpdPick, ArpuPick, SlowPick
from .artifacts.screw import FastScrew, SlowScrew
from .notemaker import Art, Boardh, Bowh
from .utility import repeat, swap


class ChorusObject:
    def __init__(self, notemaker):
        self.notemaker = notemaker
        # # # # # # # strings dur artifacts pos comment dynamic bowhand boardhand bridge
        # compose
        # TODO sometimes electronic in not arco part on?
        # TODO col legno first gone?
        # TODO gestures get really more soft?
        # TODO repeat get and put loopy loop
        # TODO split gestures in right and left hand parts
        # notate
        # TODO fix Tremolo (there is a solution by hand)

        # michael
        # TODO overpressure
        # TODO not i samuel beckett

        self.milk = (MilkOff(notemaker, 4), MilkOn(notemaker, 3),
                     MilkOn(notemaker, 2), MilkOn(notemaker, 1))
        self.screw = (FastScrew(notemaker, 4), SlowScrew(notemaker, 3),
                      SlowScrew(notemaker, 2), SlowScrew(notemaker, 1))
        self.pick = (SlowPick(notemaker, 4), ArpuPick(notemaker, 3),
                     ArpdPick(notemaker, 2), SlowPick(notemaker, 1))
        self.clb = (SlowLegno(notemaker, 4), SlowLegno(notemaker, 3),
                    FastLegno(notemaker, 2), SlowLegno(notemaker, 1))
        self.pizz = (Finger(notemaker, 4), Finger(notemaker, 3),
                     Finger(notemaker, 2), Finger(notemaker, 1))
        self.arco = (Hair(notemaker, 4), Hair(notemaker, 3),
                     Hair(notemaker, 2), Hair(notemaker, 1))

        self.arts = (self.milk, self.screw, self.pick, self.clb)
        # non mutable
        self.indices = (0, 1, 2, 3)

        # permutations
        root_perm = list(it.permutations(self.indices))
        perm1 = sorted(root_perm, key=lambda x: swap(x, 0, 1))
        perm2 = sorted(root_perm, key=lambda x: swap(x, 0, 2))
        perm3 = sorted(root_perm, key=lambda x: swap(x, 0, 3))
        self.permutations = (it.cycle(root_perm), it.cycle(perm1),
                             it.cycle(perm2), it.cycle(perm3))

    def motif(self, pizzas, stretch=1):
        # setup
        perm = tuple(next(p) for p in self.permutations)
        # artifacts
        notes1 = []
        for p0, p1 in zip(perm[0], perm[1]):
            if p1 in pizzas:
                tmp_art = self.pizz
            else:
                tmp_art = self.arts[p1]
            tmp_atom = cp.deepcopy(tmp_art[p0])
            notes1.append(tmp_atom)
        notes1 = repeat(notes1, perm[2][2], perm[2][0], perm[2][1])

        # arco
        double = cp.deepcopy(self.arco[perm[3][0]])
        notes2 = [double]
        notes2 = repeat(notes2, 0, 4, perm[3][1])
        return notes1 + notes2

    def make_measures(self, arts):
        result = []
        a = [Artifact(None, None)]
        loop = a + arts + a
        for before, current, after in zip(loop, loop[1:], loop[2:]):
            measure = current.make_measure(before, after)
            result.append(measure)
        return result

    def chorus(self, pizzas, electro, n=6):
        artifacts = []
        for i in range(n):
            tmp = self.motif(pizzas)
            artifacts.extend(tmp)
        result = self.make_measures(artifacts)
        return aj.Voice(result)

    def chorus1(self):
        return self.chorus((), 1)

    def chorus2(self):
        return self.chorus((0,), 2)

    def chorus3(self):
        return self.chorus((0, 1), 3)

    def chorus4(self):
        return self.chorus((0, 1, 2), 4)

    def chorus5(self):
        return self.chorus((0, 1, 2, 3), 5, 1)
