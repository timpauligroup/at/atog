import abjad as aj

from .notemaker import Art


def verse(nm, elec, comment):
    notes = nm.make_notes(0, (1, 1), Art.REST, comment=comment, elec=elec)
    measure = aj.Measure((4, 4))
    measure.extend(notes)
    return aj.Voice([measure])


def verse1(nm):
    notes = nm.make_notes(0, (1, 8), Art.REST, comment="light on", elec=1)
    measure = aj.Measure((1, 8))
    measure.extend(notes)
    return aj.Voice([measure])


def verse2(nm):
    v = verse(nm, 2, "")  # attack table with milk frother ca. 15 sec
    note = aj.inspect(v).leaf(0)
    aj.attach(aj.Fermata(command="longfermata"), note)
    return v


def verse3(nm):
    v = verse(nm, 3, "")  # prepare with screw at D 5ta
    note = aj.inspect(v).leaf(0)
    aj.attach(aj.Fermata(command="fermata"), note)
    return v


def verse4(nm):
    v = verse(nm, 4, "")  # prepare with pick at G D A 5ta
    note = aj.inspect(v).leaf(0)
    aj.attach(aj.Fermata(command="fermata"), note)
    return v


def verse5(nm):
    v = verse(nm, 5, "")  # use transducer ca. 30 sec
    note = aj.inspect(v).leaf(0)
    aj.attach(aj.Fermata(command="verylongfermata"), note)
    return v


def verse6(nm):
    notes = nm.make_notes(0, (1, 8), Art.REST, comment="light off")
    measure = aj.Measure((1, 8))
    measure.extend(notes)
    return aj.Voice([measure])
