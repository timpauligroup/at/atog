from ..notemaker import Art, Boardh, Bowh
from .artifact import Artifact


class Pick(Artifact):
    def instanceof(self):
        return 'pick'


class SlowPick(Pick):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
        self.atoms = {4: (((4, (4, 8), Art.BAT, 8, None, 'mf', Bowh.TREM16, Boardh.HARM, False), {}),),
                      1: (((1, (1, 12), Art.BAT, 4, None, 'f', None, None, False, 3), {}),)}

    def get(self):
        result = []
        pick = (0, (1 / 8), Art.GP)
        result.extend(self.notemaker.make_notes(*pick))
        pos = (0, (1 / 8), Art.DOWN)
        result.extend(self.notemaker.make_notes(*pos))
        return result

    def put(self):
        result = []
        pos = (0, (1 / 8), Art.UP)
        result.extend(self.notemaker.make_notes(*pos))
        slam = (0, (1 / 8), Art.PP)
        result.extend(self.notemaker.make_notes(*slam))
        return result


class ArpdPick(Pick):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
        self.atoms = {
            2: (((2, (1, 8), Art.BAT, 'x', None, 'f', Bowh.ARPD, None, True), {}),)}

    def get(self):
        result = []
        pick = (0, (1 / 8), Art.GP)
        result.extend(self.notemaker.make_notes(*pick))
        pos = (0, (1 / 8), Art.DOWN)
        result.extend(self.notemaker.make_notes(*pos))
        return result

    def put(self):
        args = (0, (1 / 8), Art.PP)
        return self.notemaker.make_notes(*args)


class ArpuPick(Pick):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
        self.atoms = {
            3: (((3, (1, 8), Art.BAT, 0, None, 'f', Bowh.ARPU), {}),)}

    def get(self):
        args = (0, (1 / 8), Art.GP)
        return self.notemaker.make_notes(*args)

    def put(self):
        result = []
        pos = (0, (1 / 8), Art.UP)
        result.extend(self.notemaker.make_notes(*pos))
        slam = (0, (1 / 8), Art.PP)
        result.extend(self.notemaker.make_notes(*slam))
        return result
