import copy as cp

from ..notemaker import Art, Boardh, Bowh
from .artifact import Artifact


class Finger(Artifact):
    def __init__(self, notemaker, string):
        super().__init__(notemaker, string)
        self.atoms = {4: (((4, (1, 8), Art.PIZZ, 0, None, 'mf'), {}),),
                      3: (((3, (1, 8), Art.PIZZ, 8, None, 'mp', None, Boardh.HARM), {}),),
                      2: (((0, 1 / 8, Art.REST), {}), ((2, (1, 8), Art.BART, 5, None, 'f'), {})),
                      1: (((1, (1, 8), Art.PIZZ, 8, None, 'p', None, Boardh.HARM), {}),)}
        self.stack = []

    def instanceof(self):
        return 'finger'

    def make_measure(self, before, after):
        if before.instanceof() == 'bow':
            def on(ls):
                result = cp.deepcopy(ls)
                result[0][1]['elec'] = 'off'
                return result
        else:
            def on(ls):
                return ls
        return super().make_measure(before, after, on)
