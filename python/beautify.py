import abjad as aj


def beautify(staff):
    for i, v in enumerate(staff):
        first_leaf = aj.inspect(v).leaf(0)
        # space for text
        textlength = aj.LilyPondLiteral(r'\textLengthOn', 'before')
        aj.attach(textlength, first_leaf)
        # tuplets
        brackets = aj.LilyPondLiteral(r'\override TupletBracket.bracket-visibility = ##t', 'before')
        aj.attach(brackets, first_leaf)
        number = aj.LilyPondLiteral(r'\override TupletNumber.text = #tuplet-number::calc-fraction-text', 'before')
        aj.attach(number, first_leaf)
        # engraver
        v.remove_commands.append("Note_heads_engraver")
        v.consists_commands.append("Completion_heads_engraver")
        v.remove_commands.append("Rest_engraver")
        v.consists_commands.append("Completion_rest_engraver")
        v.remove_commands.append("Separating_line_group_engraver")
