from enum import Enum

import abjad as aj

from .signs import *


def up(sign):
    return aj.MarkupList([up_arrow(), sign]).center_column()


def down(sign):
    return aj.MarkupList([sign, down_arrow()]).center_column()


def artmark(text):
    return aj.Markup(text, direction=aj.Up).center_align()


class Art(Enum):
    GM = artmark(up(milk_sign()))
    PM = artmark(down(milk_sign()))
    MON = artmark("on")
    MOFF = artmark("off")
    GS = artmark(up(screw_sign()))
    PS = artmark(down(screw_sign()))
    GP = artmark(up(pick_sign()))
    PP = artmark(down(pick_sign()))
    GB = artmark(up(bow_sign()))
    PB = artmark(down(bow_sign()))
    BAT = artmark("batt.")
    TRAT = artmark("tratt.")
    PIZZ = artmark("pizz.")
    BART = aj.LilyPondLiteral(r"\snappizzicato", "after")
    ARCO = artmark("arco")
    REST = artmark((r"\transparent 0"))
    CLB = artmark("c.l.b.")
    CLT = artmark("c.l.t.")
    UP = artmark(up(None))
    DOWN = artmark(down(None))


class Bowh(Enum):
    ARPU = aj.Arpeggio(direction=aj.Up)
    ARPD = aj.Arpeggio(direction=aj.Down)
    TREM32 = aj.StemTremolo(32)
    TREM16 = aj.StemTremolo(16)
    TREM8 = aj.StemTremolo(8)
    # TREMC = aj.LilyPondLiteral(r'\repeat tremolo 2', format_slot='before',)
    # aj.Tremolo(beam_count=3) # TODO trem still not implemented in abjad
    TREMC = aj.StemTremolo(64)


class Boardh(Enum):
    HARM = "harmonic"


def patch(text):
    return aj.Markup(r"\rounded-box " + text, direction=aj.Up).center_align()


class NoteMaker:
    def __init__(self):
        self.maker = aj.LeafMaker()

        self.strings = (None, 21, 7, -7, -20)

        pos = (r"\transparent 0", "4ta", "5ta", "8va")
        pos = tuple(aj.Markup(r"\italic" + x, direction=aj.Down) for x in pos)
        damp = '\\combine \\bold "O" \\path #0.2 #\'((moveto -.4 .8) (lineto 2.2 .8) (closepath) (moveto .9 -.5) (lineto .9 2.1))'
        self.positions = {
            0: pos[0],
            4: pos[1],
            5: pos[2],
            8: pos[3],
            "x": aj.Markup(damp, direction=aj.Down),
        }

        elec = [patch("patch" + str(i)) for i in range(1, 6)]
        self.electros = {
            0: None,
            1: elec[0],
            2: elec[1],
            3: elec[2],
            4: elec[3],
            5: elec[4],
            "on": patch("on"),
            "off": patch("off"),
        }

    def make_notes(
        self,
        strings,
        dur,
        artifact,
        pos=0,
        comment=None,
        dyn=None,
        bowhand=None,
        boardhand=None,
        bridge=False,
        repeat=1,
        elec=0,
    ):
        try:
            pitches = [tuple(self.strings[s] for s in strings)]
        except TypeError:
            pitches = [self.strings[strings]]
        position = self.positions[pos]
        electro = self.electros[elec]

        # special arpeggio
        if bowhand == Bowh.ARPU or bowhand == Bowh.ARPD:
            pitches = [self.strings[1:]]
        # notes
        notes = self.maker(pitches, [aj.Duration(dur)] * repeat)
        note = aj.inspect(notes).leaf(0)  # reference alias
        # hands
        if bowhand != None:
            aj.attach(bowhand.value, note)
        # artifact and pos
        aj.attach(artifact.value, note)
        aj.attach(position, note)
        if boardhand != None:
            aj.override(note).note_head.style = boardhand.value
        # other
        if comment != None:
            aj.attach(artmark(r"\tiny {\italic {" + comment + "}}"), note)
        if dyn != None:
            aj.attach(aj.Markup(r"\dynamic " + dyn, direction=aj.Down), note)
        if bridge:
            aj.override(note).note_head.style = "cross"

        if electro != None:
            aj.attach(electro, note)
        return notes

    def make_phrase(self, arglists):
        notes = []
        for args, kwargs in arglists:
            notes.extend(self.make_notes(*args, **kwargs))
        phrase = aj.Container(notes)
        return phrase

    def make_measure(self, notes, timesig=(8, 8)):
        timesig = aj.TimeSignature(timesig)
        measure = aj.Measure(timesig, [])
        measure.automatically_adjust_time_signature = True
        measure.extend(notes)
        return measure
