import abjad as aj


def eps(file, axis, size=1):
    return aj.MarkupCommand('epsfile', axis, size, aj.Scheme("icons/" + file + ".eps", force_quotes=True))


def up_arrow():
    return eps("up", '#Y', 2)


def down_arrow():
    return eps("down", '#Y', 2)


def milk_sign():
    return eps("milk", '#X', 1)


def pick_sign():
    return eps("pick", '#X', 3)


def screw_sign():
    return eps("screw", '#X', 2)


def bow_sign():
    return eps("bow", '#X', 0.75)
