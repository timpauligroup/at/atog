public class Utility{
  fun static int isIn(int element, int ls[]){
    for(0 => int i; i < ls.cap(); i++){
      if(element == ls[i]){
          return true;
      }
    }
    return false;
  }

  fun static int check_clip(UGen ugen, string label){
    ugen.last() => float last;
    if (last > 1 || last < -1){
      <<<label, "clipped:", last>>>;
      return 1;
    }
    return 0;
  }

  fun static int ftosamp(float freq){
    second / samp => float srate;
    (srate / freq)$int => int buffersize;
    if (buffersize <= 0){
      1 => buffersize;
    }
    return buffersize;
  }

  fun static float maxabs(float ls[]){
    Std.fabs(ls[0]) => float current_max;
    for (0 => int i; i < ls.cap(); i++){
      Math.max(Std.fabs(ls[i]), current_max) => current_max;
    }
    if (current_max == 0){
      1 => current_max;
    }
    return current_max;
  }

  fun static float find_octave(float freq, float min, float max){
    freq => float result;
    while(result > max){
      result / 2 => result;
    }
    if (result < min){
      0 => result;
    }
    return result;
  }
}
