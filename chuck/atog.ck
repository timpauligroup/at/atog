1 => int midi_device;
[1, 2, 3, 4, 5] @=> int patch_ctrls[];
27 => int wet_ctrl;
7 => int trans_ctrl;
0 => int in_ch_vio;
1 => int in_ch_table;
0 => int out_ch_wet;
1 => int out_ch_vio;
2 => int out_ch_table;
3 => int out_ch_trans;

// setup
// wet
WetRack wetrack;
0.75 => wetrack.g.gain;
0.25 => wetrack.g_rev.gain;
MasterRack wetrack_master;
adc.chan(in_ch_vio) => wetrack => wetrack_master => dac.chan(out_ch_wet);

// vio
Gain vio_switcher;
1 => vio_switcher.gain;
adc.chan(in_ch_vio) => vio_switcher;
MasterRack vio_master;
vio_switcher => vio_master => dac.chan(out_ch_vio);

// table
Gain table_switcher;
1 => table_switcher.gain;
adc.chan(in_ch_table) => table_switcher;
MasterRack table_master;
table_switcher => table_master => dac.chan(out_ch_table);

// transducer
SndBuf buffer => dac.chan(out_ch_trans);
me.sourceDir() + "/nightingale.wav" => string filename;
filename => buffer.read;
1 => buffer.loop;
0 => buffer.gain;

// methods
// logic
fun void onoff(float wet){
  1 - wet => float dry;
  wet => wetrack.switcher.gain;
  wet => wetrack.sine_switcher.gain;
  dry => vio_switcher.gain;
  dry => table_switcher.gain;
  if (wet == 0){
    0 => wetrack.fftwave.op;
  }
  else{
    1 => wetrack.fftwave.op;
  }
  if (dry == 0){
    0 => vio_switcher.op;
    0 => table_switcher.op;
  }
  else{
    1 => vio_switcher.op;
    1 => table_switcher.op;
  }
}

fun float pedal(int cc){
  16.0 => float bottom;
  112.0 => float top;
  float result;
  if (cc < bottom){
    0 => result;
  }
  else if (cc > top){
    1 => result;
  }
  else{
    cc / (top - bottom) => result;
  }
  return result * result * result;
}

// midi setup
MidiIn min;
if(!(min.open(midi_device))){
  me.exit();
}
MidiMsg msg;

while(true) {
  while(min.recv(msg)){
    if(Utility.isIn(msg.data2, patch_ctrls)){
      if (msg.data2 == 1){
        wetrack.patch1();
      }
      if (msg.data2 == 2){
        wetrack.patch2();
      }
      if (msg.data2 == 3){
        wetrack.patch3();
      }
      if (msg.data2 == 4){
        wetrack.patch4();
      }
      if (msg.data2 == 5){
        wetrack.patch5();
      }
    }
    if(msg.data2 == wet_ctrl){
      onoff(pedal(msg.data3));
    }
    if(msg.data2 == trans_ctrl && wetrack.patch == 5){
      pedal(msg.data3) => buffer.gain;
    }
  }
  256::samp => now;
}
