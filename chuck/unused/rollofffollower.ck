public class RollOffFollower extends Chubgraph
{
  // patch
  inlet => FFT fft =^ RollOff rolloff => blackhole;
  Step dc => Envelope env => outlet;
  // set parameters
  1024 => fft.size;
  Windowing.hann(fft.size()) => fft.window;
  1 => dc.next;
  fft.size()::samp => env.duration;
  float _rolloffresult[];
  (second / samp) / 2 => float nyquist;

  // spork
  spork ~ pitchfollower_process();

  fun void pitchfollower_process(){
    while (true){
      rolloff.upchuck().fvals() @=> _rolloffresult;
      _rolloffresult[0] * nyquist=> env.target;;
      fft.size()::samp => now;
    }
  }
}
