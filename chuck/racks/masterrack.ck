public class MasterRack extends Chubgraph
{
    HPF hpf;
    40 => hpf.freq;
    LPF lpf;
    19000 => lpf.freq;
    Dyno limiter;
    limiter.limit();
    inlet => hpf => lpf => limiter => outlet;
}
