import abjad as aj

import python.beautify as beautify
import python.verse as verse
from python.chorus import ChorusObject
from python.notemaker import NoteMaker

nm = NoteMaker()
chorus = ChorusObject(nm)

notes = [verse.verse1(nm),
         chorus.chorus1(), verse.verse2(nm),
         chorus.chorus2(), verse.verse3(nm),
         chorus.chorus3(), verse.verse4(nm),
         chorus.chorus4(), verse.verse5(nm),
         chorus.chorus5(), verse.verse6(nm)]

aj.attach(aj.Fermata(command='longfermata'), aj.inspect(notes[-2]).leaf(-3))
staff = aj.Staff(notes)
# score
# instrument
order = ['right', 'E', 'A', 'D', 'G', 'left']
order = [aj.Markup(x) for x in order]
aj.setting(staff).instrument_name = aj.Markup.column(order)
# 4 lines
aj.override(staff).staff_symbol.line_count = 4
aj.override(staff).staff_symbol.line_positions = '#\'(-12 -4 4 12)'
aj.override(staff).TextScript.staff_padding = 2

first_leaf = aj.inspect(staff).leaf(0)
# clef
clef = aj.Clef('percussion')
aj.attach(clef, first_leaf)
# tempo
tempo = aj.MetronomeMark((1, 4), 50)
aj.attach(tempo, first_leaf)
# numeric
numeric = aj.LilyPondLiteral(r'\numericTimeSignature', 'before')
aj.attach(numeric, first_leaf)
# beautify
beautify.beautify(staff)

# score
score = aj.Score([staff])
score.add_final_bar_line()

# header
lilypond_file = aj.LilyPondFile.new(score)
title = 'atog'
lilypond_file.header_block.title = aj.Markup(title)
subtitle = 'electric violin, objects, electronics'
lilypond_file.header_block.subtitle = aj.Markup(subtitle)
lilypond_file.header_block.tagline = aj.Markup(
    'gitlab.com/timpauliart/at/atog')
lilypond_file.header_block.composer = aj.Markup('Tim Pauli')
lilypond_file.header_block.copyright = aj.Markup('2019')

# proportional notation
# proportional = '\context {\Score proportionalNotationDuration = #(ly:make-moment 1/2)}'
# lilypond_file.layout_block.items.append(proportional)
minimum = 'system-system-spacing #\'minimum-distance = #28'
lilypond_file.paper_block.items.append(minimum)

# render
folder = 'build/'
aj.persist(lilypond_file).as_pdf(folder + title + '_score.pdf')
aj.persist(lilypond_file).as_midi(folder + title + '.midi')
